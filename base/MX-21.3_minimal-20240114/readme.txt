README.txt

2023-04-20 03:17 UTC
updated: 2024-01-14 06:46 UTC

The minimal version is intended for who needs only core components to start with MX Linux.

For users' convenience, this image have applied all latest updates by upstreams so far. Be noted that English is default and the only one language supported. For use in other language, you yourself customize it. Also, "dkms" (Dynamic Kernel Module Support) was removed, so install dkms package later if you need it.

The original MX_minimal ISOs are available from below:
https://sourceforge.net/projects/mx-linux/files/Community_Respins/

Best regards.

